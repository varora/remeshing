from scipy.spatial import distance


def calculate_hausdorff_distance(mesh1, mesh2):
    """
    This function calculates the Hausdorff distance between two meshes.
    :param mesh1: vertices of the first mesh Nx3
    :param mesh2: vertices of the second mesh Mx3
    :return: distances: the Hausdorff distance between the two meshes
    """
    distances = distance.directed_hausdorff(mesh1, mesh2)
    return distances