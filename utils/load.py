import glob
import bpy
import numpy as np
from tqdm import tqdm
from utils.log import block_print, enable_print


def get_filepaths(root: str, extension: str = '.obj'):
    """
    Get the filepaths of all the .obj files in a directory using glob.
    :param root: the root path of the directory
    :param extension: the extension of the files to get
    :return: list of filepaths
    """
    assert extension in ['.obj', '.ply']
    filepaths = []
    for filepath in glob.iglob(root + '**/*'+extension, recursive=True):
        filepaths.append(filepath)
    return filepaths


def load_ply(ply_path):
    """
    Load a PLY mesh.
    :param path: the path to the PLY mesh
    :return: the vertices and faces of the mesh
    """
    # import the mesh
    bpy.ops.import_mesh.ply(filepath=ply_path)
    # select all the objects in the scene
    bpy.ops.object.select_all(action='SELECT')

    b_object = bpy.context.object

    vertices = np.array([v.co for v in b_object.data.vertices])  # get coords of vertices
    faces = []
    for face in b_object.data.polygons:
        faces.append([v for v in face.vertices])

    return vertices, faces


def load_obj(path):
    """
    Load a .obj file using bpy.
    :param path: the path to the .obj file
    :return: the Blender object
    """
    block_print()
    bpy.ops.import_scene.obj(filepath=path)
    enable_print()
    return bpy.context.selected_objects[0]


def load_objs(paths: list):
    """
    Load multiple .obj files using bpy.
    :param paths: list of paths to the .obj files
    :return: void
    """
    total = len(paths)
    for path in tqdm(paths, desc="Loading objs", total=total):
        load_obj(path)


