import argparse
from utils.log import log_print


def print_arguments(args):
    """
    Print the arguments.
    :param args: the arguments
    :return: void
    """
    log_print("Arguments:")
    for arg in vars(args):
        log_print("\t" + arg + ": " + str(getattr(args, arg)))


def get_arguments_evaluate():
    """
    Get the arguments for the evaluate script.
    :return: the arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--root_path", help="the root path",
                        default="/home/varora/PythonProjects/OSSO/remesh", type=str)
    parser.add_argument("-s", "--ref_ply_filename", help="the reference mesh filename",
                        default="osso_inference_topo_from_objs", type=str)
    parser.add_argument("-o", "--outputs_relative_path", help="the path for the output folder relative to root",
                        default="./outputs/", type=str)
    parser.add_argument("-v", "--visualize", help="whether to visualize the mesh", action="store_true")
    arguments = parser.parse_args()

    # print_arguments(arguments)

    return arguments


def get_arguments_main():
    """
    Get the arguments for the script.
    :return: the arguments
    """
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--root_path", help="the root path",
                        default="/home/varora/PythonProjects/OSSO/remesh", type=str)
    parser.add_argument("-s", "--sym_output_filename", help="the symmetric mesh output filename",
                        default="osso_inference_topo_from_objs", type=str)
    parser.add_argument("-o", "--obj_relative_path", help="the path for the .obj files relative to root",
                        default="obj/", type=str)
    parser.add_argument("-v", "--visualize", help="whether to visualize the mesh", action="store_true")
    parser.add_argument("-f", "--force_recompute", help="whether to force recomputation of the symmetric mesh",
                        action="store_true")
    parser.add_argument("-d", "--decimate", help="whether to decimate the mesh", action="store_true")
    parser.add_argument("-dr", "--decimation_ratio", help="the ratio to decimate the mesh by", type=float, default=0.5)
    parser.add_argument("-di", "--ignore_decimation_objects", help="list of objects to ignore for decimation",
                        nargs="*", default=[], type=str)
    parser.add_argument("-l", "--compute_losses", help="whether to compute the losses between two meshes", action="store_true")
    arguments = parser.parse_args()

    return arguments


def get_arguments(evaluate: bool = False):
    """
    Get the arguments for the script.
    :return: the arguments
    """
    if evaluate:
        return get_arguments_evaluate()
    else:
        return get_arguments_main()
