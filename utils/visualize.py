import polyscope as ps
import os.path as osp
from utils.log import log_print
import bpy
import numpy as np

def visualize_mesh_distances(ref: dict, output: dict, output_img_path: str):
    """
    This function loads the reference mesh and the output mesh, overlays them, and visualizes the surface distances
    between the two meshes using a color bar. The color bar is normalized to the maximum distance between the two
    meshes.
    :param ref:
    :param output_path:
    :return:
    """
    # render the reference mesh
    ps_mesh_ref = ps.register_surface_mesh(
        ref['name'], ref['vertices'], ref['faces'], transparency=0.5, color=[0.0, 0.0, 1.0],
        edge_color=[0.0, 0.0, 1.0], edge_width=0.1,
    )
    # render the output mesh
    ps_mesh_output = ps.register_surface_mesh(
        output['name'], output['vertices'], output['faces'], transparency=0.5, color=[1.0, 0.0, 0.0],
        edge_color=[1.0, 0.0, 0.0], edge_width=0.1,
    )
    # save the plot as png
    ps.set_screenshot_extension(".png")
    ps.set_navigation_style('turntable')
    ps.set_up_dir('z_up')
    # plot the two meshes together
    ps.show()
    output_img_path = osp.join(output_img_path, "REF" + '_' + output['name'].replace('ply', 'png'))
    ps.screenshot(filename=output_img_path, transparent_bg=False)
    log_print(f"Saved image to {output_img_path}")
    ps.remove_all_structures()


def visualize_ply(ply_path, name="ply_mesh"):
    """
    Visualize a .ply mesh using Blender's Python API and polyscope.
    :param ply_path: str, the path to the .ply file
    :return: void
    """
    # import the mesh
    bpy.ops.import_mesh.ply(filepath=ply_path)
    log_print("Imported mesh from {}".format(ply_path))

    bpy.ops.object.select_all(action='SELECT')

    b_object = bpy.context.object

    vertices = np.array([v.co for v in b_object.data.vertices])  # get coords of vertices
    faces = []
    for face in b_object.data.polygons:
        faces.append([v for v in face.vertices])
    # visualize the mesh
    ps.register_surface_mesh(name, vertices, faces, edge_width=1.0, enabled=True)
    ps.set_navigation_style('turntable')
    ps.set_up_dir('z_up')
    output_img_path = osp.join(osp.dirname(ply_path), "imgs",
                               osp.basename(ply_path).replace('ply', 'png'))
    ps.screenshot(filename=output_img_path, transparent_bg=False)
    ps.show()


def visualize_multiple_meshes(all_objects: list):
    """
    Visualize multiple meshes in Blender.
    :param all_objects: list of Blender objects
    :return: void
    """
    for obj in all_objects:
        visualize_bpy_mesh(obj, multiple=True)
    ps.show()
    # clear up polyscope
    ps.remove_all_structures()


def visualize_bpy_mesh(b_object, multiple=False):
    """
    Visualize a mesh using Blender's Python API and polyscope.
    :param b_object: the Blender object
    :param multiple: whether to visualize multiple meshes
    :return: void
    """
    name = b_object.name
    if (b_object.type == 'MESH') and (name != 'Cube'):
        try:
            vertices = np.array([v.co for v in b_object.data.vertices])  # get coords of vertices
            faces = np.array([f.vertices for f in b_object.data.polygons])  # get indices of vertices in each face
            # visualize the mesh
            ps.register_surface_mesh(name, vertices, faces, edge_width=1.0, enabled=True)
            if not multiple:
                ps.show()
        except ValueError:
            log_print("The object {} has no vertices. Skipping.".format(name))
    else:
        if name == 'Cube':
            log_print("The object {} is the default cube. Skipping.".format(name))
        else:
            log_print("The object {} is not a mesh. Skipping.".format(name))