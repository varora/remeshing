## Description
![Python](https://img.shields.io/badge/Python->=3.10-Blue?logo=python) 
![Blender](https://img.shields.io/badge/bpy-%3E=3.5.0-Orange?logo=blender)

This repository contains the code for the symmetrization and decimation of the bones of the inference skeleton from the OSSO project. The code uses Blender's API for python.
For now, it is tuned for the OSSO project, but it can be easily adapted to any input meshes. A wavefront .obj file of the complete mesh to be symmetrized can be used instead of those of the individual bones, for example.
- Symmetrization is performed by default (bisection and mirroring).
- Optionally, the symmetrized meshes can be decimated by a given ratio (using bpy Edge Contraction https://www.cs.cmu.edu/~./garland/Papers/quadrics.pdf). Default behaviour is to decimate all bones. For example, a decimation ratio of 0.5 would reduce the number of vertices by 50% while aiming to keep the surface geometry intact as much as possible. Below are example images for a reduction of 70%.
- Optionally, given a list of bones to ignore, the decimation is performed only on the remaining bones.

## Tested using
- conda (version : 23.3.1)
- python (version : 3.10.9)
- platform : linux-64 (Ubuntu 22.04.2 LTS)

## Installation
Follow these steps to set up the conda environment and run the code:
1. Clone the repository:
`git clone https://gitlab.inria.fr/varora/remeshing.git`
2. Change into the project directory
3. Create a new conda environment:
`conda create -n remesh_blender python=3.10`
Note: Blender 3.0 requires Python 3.10
4. Activate the conda environment:
`conda activate remesh_blender`
5. Install the required packages:
```pip install -r requirements.txt```
Main dependencies: `numpy`, `scipy` for metric computation, `bpy` for Blender scripting and `polyscope` for visualization.

## Pre-requisites
- .obj files of the meshes of the bones in the inference skeleton. Download from: https://mybox.inria.fr/d/1470ce327ef54b9582c1/
- expected folder structure:
```
root_path
├── bpy_sym_dec.py
├── bpy_sym_dec_evaluate.py
├── requirements.txt
├── README.md
├── utils
│   ├── __init__.py
│   ├── metrics.py
│   ├── arguments.py
│   ├── visualize.py
│   ├── load.py    
│   ├── log.py
├── obj
│   ├── obj1.obj
│   ├── obj2.obj
│   ├── ...
│   └── objN.obj
└── output (created by the script)
    ├── imgs
    │   ├── REF_osso_inference_topo.png
    │   ├── ...
    │   ├── .png
    ├── skeleton.ply
    ├── skeleton_sym.ply
```

## Usage
We load the meshes of different bones from the .obj files, symmetrize them and optionally decimate them. 
The output is a .ply file with the symmetrized and decimated bones stored in the `./outputs` folder.
#### Symmetrize:
    python bpy_sym_dec.py -r <root_path> -s <sym_output_filename> -o <obj_relative_path> -v -f
#### Symmetrize and decimate:
```
Arguments:
    root_path: /home/varora/PythonProjects/OSSO/remesh
    sym_output_filename: osso_inference_topo_from_objs
    obj_relative_path: obj/
    visualize: True
    force_recompute: True
    decimate: True
    decimation_ratio: 0.3
    ignore_decimation_objects: ['Left_femur', 'Right_femur']
```
    python bpy_sym_dec.py -r <root_path> -s <sym_output_filename> -o <obj_relative_path> -v -f -d -dr <decimate_ratio> -di <ignore_decimation_objects_list>
Having saved the symmetrized/decimated meshes, we compare them with the reference meshes to compute the metrics and visualize the results together.
#### Evaluate:
```
Arguments:
    root_path: /home/varora/PythonProjects/OSSO/remesh
    ref_ply_filename: osso_inference_topo_from_objs
    outputs_relative_path: ./outputs/
    visualize: False
    force_recompute: False
```
    python bpy_sym_dec_evaluate.py -r <root_path> -s <ref_ply_filename> -o <outputs_relative_path> -v

## Results
Download output example .ply files from: https://mybox.inria.fr/d/9974866948fe47789167/

Overlaying Symmetrized and Reference meshes:
![ref-sym](outputs/imgs/REF_osso_inference_topo_from_objs_sym.ply.png)

Overlaying (Symmetrized + Decimated_0.3) and reference meshes:
![ref-sym_dec03](outputs/imgs/REF_osso_inference_topo_from_objs_sym_dec_0.3.ply.png)

(Symmetrized + Decimated_0.3 + Left_Right_Femur_Mesh_Intact:
![](outputs/imgs/osso_inference_topo_from_objs_sym_dec_0.3_Left_femur_Right_femur.png)

## TODO
- [ ] Add more quantitive metrics for evaluation (only Hausdorff distance for now). Useful?
- [ ] Color distance map visualization for evaluation (only overlaying meshes for now)
- [ ] Output mesh inspection (only visualization of the symmetrized and decimated meshes for now)
- [ ] Optimize for decimation ratio
- [ ] Improve naming convention for output files
- [ ] Generalize for any input mesh (not just the OSSO project), including .obj and .ply files
