"""
This script loads the reference mesh and the output meshes, computes the hausdorff distance between the two meshes,
and visualizes them and saves the image iteratively in relation to the reference mesh.
Usage:
    python bpy_sym_dec_evaluate.py -r <root_path> -s <ref_ply_filename> -o <outputs_relative_path> -v
"""

import polyscope as ps
import os.path as osp
import os
import sys
import numpy as np
from utils.log import log_print, set_logging
from utils.load import load_ply, get_filepaths
from utils.metrics import calculate_hausdorff_distance
from utils.visualize import visualize_mesh_distances
from utils.arguments import get_arguments, print_arguments


def compute_differences(ref: dict = None, output_path: str = None):
    """
    This function loads the reference mesh and the output mesh, computes hausdorff distance between the two meshes,
    and visualizes them and saves the image.
    :param ref: dictionary containing the reference mesh name, vertices, and faces
    :param output_path: output mesh path
    :return:
    """
    assert ref['vertices'] is not None, "Reference mesh is None"
    assert output_path is not None, "Output path is None"
    # get reference mesh vertices
    ref_vertices = ref['vertices']

    # load the output mesh
    output_vertices, output_faces = load_ply(output_path)

    # get the name of the reference mesh and the output mesh
    ref_name = osp.basename(ref['filepath'])
    output_name = osp.basename(output_path)

    # get output image path
    output_img_path = osp.join(osp.commonpath((ref["filepath"], output_path)), "imgs")
    # create the output image path if it does not exist
    os.makedirs(output_img_path, exist_ok=True)

    # convert the vertices to float16 to save memory
    ref_vertices, output_vertices = ref_vertices.astype(np.float16), output_vertices.astype(np.float16)

    # calculate the hausdorff distance between the two meshes
    hausdorff_dist = calculate_hausdorff_distance(ref_vertices, output_vertices)
    log_print(f"Hausdorff distance b/w reference mesh and {output_name}: {hausdorff_dist}")

    # create the reference and output dictionaries
    ref = {"name": ref_name, "vertices": ref_vertices, "faces": ref['faces']}
    output = {"name": output_name, "vertices": output_vertices, "faces": output_faces}

    # visualize the two meshes and save the image
    visualize_mesh_distances(ref, output, output_img_path)


if __name__ == "__main__":
    # initialize polyscope
    ps.init()

    # get the arguments
    args = get_arguments(evaluate=True)
    # set the logging
    log_filename = osp.join(args.root_path, args.outputs_relative_path, 'evaluate.log')
    set_logging(filename=log_filename)

    # print the arguments
    print_arguments(args)

    # get the outputs directory
    outputs_dir = osp.join(args.root_path, args.outputs_relative_path)

    # make sure an output directory exists
    if not osp.exists(args.root_path):
        sys.exit("The 'outputs' directory does not exist at {}. Generate the output meshes first.".format(args.root_path))

    # get the list of all the .ply files in the outputs directory
    filepaths = get_filepaths(outputs_dir, extension='.ply')

    # get the reference mesh filepath
    ref_filepath = osp.join(args.root_path, args.outputs_relative_path, args.ref_ply_filename + '.ply')
    log_print(f"\nLoading reference mesh {osp.basename(ref_filepath)}")
    ref_vertices, ref_faces = load_ply(ref_filepath)
    ref = {'filepath': ref_filepath,
        'vertices': ref_vertices,
        'faces': ref_faces
    }

    # remove the reference mesh from the list of filepaths
    if ref_filepath in filepaths:
        filepaths.remove(ref_filepath)

    log_print(f"\nComputing Hausdorff distance and plotting meshes between reference mesh and all other meshes:")

    # compute the hausdorff distance between the reference mesh and all the other meshes and plot them
    for output_filepath in filepaths:
        compute_differences(ref, output_filepath)