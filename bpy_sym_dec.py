"""
This script uses .obj files of the bone parts of the OSSO inference skeleton.
It uses Blender's Python API to import the files, bisect the skeleton along the x-axis, mirror it, and export it as a
new .ply mesh. It optionally decimates the mesh before exporting it.

expected folder structure:
root_path
├── bpy_sym_dec_multi_obj.py
├── obj
│   ├── obj1.obj
│   ├── obj2.obj
│   ├── ...
│   └── objN.obj
└── output (created by the script)
    ├── skeleton.ply
    ├── skeleton_sym.ply

Default behavior is to symmetrize the input meshes and save them as .ply files in the output directory as a dense mesh.
Optionally, the script can decimate the meshes before saving the symmetrized light-weight meshes (--decimate).
Usage:
Symmetrize:
    python bpy_sym_dec.py -r <root_path> -s <sym_output_filename> -o <obj_relative_path> -v -f
Symmetrize and decimate:
    python bpy_sym_dec.py -r <root_path> -s <sym_output_filename> -o <obj_relative_path> -v -f -d -dr <decimate_ratio> -di <ignore_decimation_objects_list>
"""

import bpy
import polyscope as ps
import os.path as osp
import os
from tqdm import tqdm
import sys
from utils.log import log_print, set_logging
from utils.load import load_objs, get_filepaths
from utils.arguments import get_arguments, print_arguments
from utils.visualize import visualize_ply, visualize_multiple_meshes


def mirror_objects():
    """
    This function mirrors all the objects in the scene by iterating through each in a way that avoids
    "Process finished with exit code 139 (interrupted by signal 11: SIGSEGV)" error. Not the most elegant solution, but
    it works. Basically we want to avoid modifying the objects in bpy.data.objects directly.
    :return: void
    """
    log_print("\nMirroring the mesh...")
    objects_to_remove = []  # List to store the original objects for removal
    # Iterate through the objects in the scene
    total_objects = len([obj for obj in bpy.data.objects if obj.type == 'MESH'])
    t = tqdm(bpy.data.objects, total=total_objects)
    for obj in t:
        t.set_description(f"Mirroring {obj.name}")
        if obj.type == 'MESH':
            try:
                # Duplicate the object
                bpy.ops.object.select_all(action='DESELECT')
                obj.select_set(True)
                bpy.context.view_layer.objects.active = obj
                bpy.ops.object.duplicate(linked=False)
                mirrored_obj = bpy.context.object

                # Apply the mirror modifier to the duplicate object
                bpy.context.view_layer.objects.active = mirrored_obj
                bpy.ops.object.modifier_add(type='MIRROR')
                mirrored_obj.modifiers["Mirror"].use_axis[0] = True  # Enable the relevant axis for mirroring
                mirrored_obj.modifiers["Mirror"].use_bisect_axis[0] = True  # Enable bisect for mirroring
                bpy.ops.object.modifier_apply(modifier="Mirror")  # Apply the mirror modifier

                # Rename the mirrored object
                mirrored_obj.name = obj.name + "_mirrored"

                # Add the original object to the removal list
                objects_to_remove.append(obj)

            except Exception:
                pass
        else:
            log_print("The object {} is not a mesh. Skipping.".format(obj.name))

    # Remove the original objects
    for obj in objects_to_remove:
        bpy.data.objects.remove(obj, do_unlink=True)

    # Rename the mirrored objects as the original objects
    for obj in bpy.data.objects:
        name = obj.name.split("_")[0]
        if obj.type == 'MESH':
            obj.name = name

    log_print("Mirroring completed. Original objects removed.\n")


def decimate_objects(decimation_ratio: float = 0.5, ignore_objects: list = []):
    """
    This function decimates all the objects in the scene by iterating through each by
    using duplicates of the original objects. Additionally, we can pass a list of objects to be ignored.
    :param decimation_ratio: the ratio of vertices to be decimated
    :param ignore_objects: a list of objects to be ignored for decimation
    :return: void
    """
    log_print(f"\nDecimating the mesh by {decimation_ratio}...")
    if not ignore_objects:
        log_print("No objects to ignore for Decimation.")
    else:
        log_print(f"Objects to ignore for Decimation: {ignore_objects}")
    objects_to_remove = []  # List to store the original objects for removal
    # Iterate through the objects in the scene
    total_objects = len([obj for obj in bpy.data.objects if obj.type == 'MESH'])
    t = tqdm(bpy.data.objects, total=total_objects)
    for obj in t:
        t.set_description(f"Decimating {obj.name}")
        if obj.type == 'MESH':
            try:
                if obj.name not in ignore_objects:
                    # Duplicate the object
                    bpy.ops.object.select_all(action='DESELECT')
                    obj.select_set(True)
                    bpy.context.view_layer.objects.active = obj
                    bpy.ops.object.duplicate(linked=False)
                    decimated_obj = bpy.context.object

                    # Apply the decimate modifier to the duplicate object
                    bpy.context.view_layer.objects.active = decimated_obj
                    bpy.ops.object.modifier_add(type='DECIMATE')
                    decimated_obj.modifiers["Decimate"].ratio = decimation_ratio  # Set the decimation ratio
                    bpy.ops.object.modifier_apply(modifier="Decimate")  # Apply the decimate modifier

                    # Rename the decimated object
                    decimated_obj.name = obj.name + "_decimated"

                    # Add the original object to the removal list
                    objects_to_remove.append(obj)

            except Exception:
                pass
        else:
            log_print("The object {} is not a mesh. Skipping.".format(obj.name))

    # Remove the original objects
    for obj in objects_to_remove:
        bpy.data.objects.remove(obj, do_unlink=True)

    # Rename the decimated objects as the original objects
    for obj in bpy.data.objects:
        name = obj.name.split("_")[0]
        if obj.type == 'MESH':
            obj.name = name

    log_print("Decimation completed. Original objects removed.\n")

def bisect_mesh():
    """
    Bisect the mesh along the x-axis.
    :return: void
    """
    log_print("\nBisecting the mesh along the x-axis...")
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.mode_set(mode='EDIT')  # set the context object to edit mode
    bpy.ops.mesh.select_all(action='SELECT')  # select all vertices of the context object
    # bisect the mesh along the x-axis
    bpy.ops.mesh.bisect(plane_co=(0.0, 0.0, 0.0), plane_no=(1.0, 0.0, 0.0), use_fill=False,
                        clear_inner=True, clear_outer=False, threshold=0.0001, xstart=0, xend=0, ystart=0, yend=0)
    bpy.ops.object.mode_set(mode='OBJECT')  # set the context object back to object mode
    log_print("Bisection completed.")


def cleanup_mesh():
    """
    Clean up the mesh by removing the inner vertices and edges.
    :return:
    """
    # clean up the mesh
    log_print("\nCleaning up the mesh...")
    bpy.ops.object.select_all(action='SELECT')  # select all vertices of the context object
    bpy.ops.object.mode_set(mode='EDIT')  # set the context object to edit mode
    bpy.ops.mesh.select_all(action='SELECT')  # select all vertices of the context object
    bpy.ops.mesh.remove_doubles(threshold=0.0001, use_unselected=False)
    bpy.ops.object.mode_set(mode='OBJECT')  # set the context object to object mode
    log_print("Cleanup completed.\n")


def symmetrize_mesh(all_objects: list, sym_filename: str, decimate: dict = None):
    """
    Symmetrize a mesh by bisecting it along the x-axis and mirroring it. And then save it as a new .ply file.
    Optionally, we can pass a dictionary containing the decimation ratio and the list of objects to be ignored
    for decimation and thus add decimation to the pipeline.
    :param decimate: a dictionary containing the decimation ratio and the list of objects to be ignored for decimation
    :param all_objects: the list of all the objects in the scene
    :param sym_filename: the path to the output symmetric .ply file
    :return: void
    """
    # print the names of all the objects in the scene
    log_print(f"Objects in the scene: {bpy.data.objects.keys()}")

    # select any object as the active object
    bpy.context.view_layer.objects.active = all_objects[2]

    # decimate the mesh
    if decimate["decimate"]:
        decimate_objects(decimation_ratio=decimate["decimation_ratio"],
                        ignore_objects=decimate["ignore_decimation_objects"])

    # bisect the mesh
    bisect_mesh()

    # mirror the objects
    # iteratively change the active object and mirror it
    mirror_objects()

    # cleanup the mesh
    cleanup_mesh()

    # export the mesh
    bpy.ops.export_mesh.ply(filepath=sym_filename, check_existing=True)

    # Cleanup the scene
    log_print("\nCleaning up the scene...")
    bpy.ops.object.select_all(action='DESELECT')
    bpy.ops.object.select_by_type(type='MESH')
    bpy.ops.object.delete()
    log_print("Finished.")


if __name__ == "__main__":
    # initialize polyscope
    ps.init()

    # get the arguments
    args = get_arguments()

    decimation = {
        "decimate": args.decimate,
        "decimation_ratio": args.decimation_ratio,
        "ignore_decimation_objects": args.ignore_decimation_objects
    }

    if args.compute_losses:
        sys.exit(0)


    # get the path to the folder containing the .obj files of the skeleton
    obj_path = osp.join(args.root_path, args.obj_relative_path)

    # make sure an output directory exists
    if not osp.exists(args.root_path):
        os.makedirs(osp.join(args.root_path + "outputs"), exist_ok=True)

    # get the path to the output .ply file to be created
    output_path = osp.join(args.root_path, "outputs", args.sym_output_filename)
    output_mesh_path = output_path + ".ply"
    output_mesh_path_sym = output_path + "_sym.ply"

    # adjust output filename if decimation is enabled
    if decimation["decimate"]:
        if not decimation["ignore_decimation_objects"]:
            output_mesh_path_sym = output_mesh_path_sym.replace("_sym", "_sym_dec"+f"_{decimation['decimation_ratio']}")
        else:
            ignore_string = '_'.join(decimation['ignore_decimation_objects'])
            output_mesh_path_sym = output_mesh_path_sym.replace("_sym", "_sym_dec"+f"_{decimation['decimation_ratio']}_{ignore_string}")

    log_filename = "".join(output_mesh_path_sym.split(".")[:-1]) + ".log"
    set_logging(filename=log_filename)

    print_arguments(args)

    if not args.visualize:
        log_print(
            "\nInput and output mesh visualization disabled. Use --visualize arg to visualize the mesh.\n"
        )

    # check if the output symmetric .ply file already exists and exit if it does
    if osp.exists(output_mesh_path_sym) and not args.force_recompute:
        log_print(f"Symmetric .ply file already exists at {output_mesh_path_sym}. Use --force_recompute arg. Exiting...")
        sys.exit(0)

    # get the list of all the .obj files in the obj_path
    filepaths = get_filepaths(obj_path)

    # list of strings to ignore in the filepaths
    ignored_objects = ['skin', 'disk', 'tooth', 'nasal', 'lacrimal']
    # if any part of the filepath string contains any of the strings in ignored_objects, ignore that filepath
    filepaths_filtered = [filepath for filepath in filepaths
                          if not any(ignored_object in filepath.lower() for ignored_object in ignored_objects)]

    # remove the default blender objects
    bpy.data.objects.remove(bpy.data.objects["Cube"])
    bpy.data.objects.remove(bpy.data.objects["Light"])
    bpy.data.objects.remove(bpy.data.objects["Camera"])

    # load the objects: the bones of the skeleton
    load_objs(filepaths_filtered)

    # select all the objects
    bpy.ops.object.select_all(action='SELECT')

    # export the mesh
    bpy.ops.export_mesh.ply(filepath=output_mesh_path, check_existing=True)

    # get a list of all the objects in the scene
    objects_list = bpy.context.selected_objects

    log_print("\nNumber of objects ignored: {}".format(len(filepaths) - len(filepaths_filtered)))
    log_print("Following objects were ignored: {}".format(ignored_objects))
    log_print("\nNumber of objects loaded: {}".format(len(objects_list)))

    # visualize the loaded objects: skeleton
    if args.visualize:
        log_print("\nVisualizing the loaded objects...")
        visualize_multiple_meshes(objects_list)

    symmetrize_mesh(objects_list, output_mesh_path_sym, decimation)

    # visualize the symmetrized skeleton
    if args.visualize:
        log_print("\nVisualizing the symmetrized mesh...")
        visualize_ply(output_mesh_path_sym)
